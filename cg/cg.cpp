#include "cg.h"

int cg(const double *A, const MKL_INT *ia, const MKL_INT *ja, const MKL_INT *n,
       double *x, const double *b,
       const double tol, int &iter)
{
  // A.  values:   Contains the nonzero entries of the matrix A.
  // ia. columns:  Contains the column indices for each nonzero element of the
  //               matrix A. nnz
  // ja. rowIndex: Contains the indices of elements in the array A. (n+1).

  char uplo = 'l'; // lower triangular part of the matrix A is used.

  // Allocate memory aligned on a 64-byte boundary for better performance
  double *r, *p, *w;
  r = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(r, "r");
  p = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(p, "p");
  w = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(w, "w");

  double alpha, beta,
    rho, rho_old,
    threshold;

  int imax = iter;

  // r = b - A*x
  // *uplo , *m , *a , *ia , *ja , *x , *y 
  mkl_cspblas_dcsrsymv(&uplo, n, A, ja, ia, x, r); // r = A*x
  cblas_daxpby(*n, 1.0, b, 1, -1.0, r, 1); // r = b - r

  rho = cblas_ddot(*n, r, 1, r, 1); // rho = (r,r)
  iter = 1;

  threshold = tol * cblas_dnrm2(*n, b, 1);
  while (sqrt(rho) > threshold && iter < imax)
    {
      if (iter == 1)
        {
          cblas_dcopy(*n, r, 1, p, 1); // p = r
        }
      else
        {
          beta = rho / rho_old;
          cblas_daxpby(*n, 1.0, r, 1, beta, p, 1); // p = r + beta*p
        }

      mkl_cspblas_dcsrsymv(&uplo, n, A, ja, ia, p, w); // w = A*p

      alpha = rho / cblas_ddot(*n, p, 1, w, 1); // alpha = rho / (p,w)
      cblas_daxpy(*n, alpha, p, 1, x, 1); // x = alpha*p + x || x = x + alpha*p
      cblas_daxpy(*n, -alpha, w, 1, r, 1); // r = -alpha*w + r || r = r - alpha*w

      rho_old = rho;
      rho = cblas_ddot(*n, r, 1, r, 1); // rho = (r,r)

      ++iter;
    }
}
