#ifndef MTX_PROC_H
#define MTX_PROC_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/* This function process the .mtx file containing the matrix entries.
 */
void mtx_proc(FILE *, int &, int &);

#endif
