#ifndef CG_H
#define CG_H

#include <stdio.h>
#include <math.h>

#include "mkl.h"

#include "chk_alloc.h"

int cg(const double *A, const MKL_INT *ia, const MKL_INT *ja, const MKL_INT *n,
       double *x, const double *b,
       const double tol, int &iter);

#endif
