#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "mkl.h"

#include "cg.h"
#include "chk_alloc.h"
#include "mtx_proc.h"
#include "opt.h"
#include "stats.h"
#include "utils.h"


int main(int argc, char **argv)
{
  srand(time(NULL));
  int n, nz;

  FILE *f_A;

  // char transa = 'n';
  char uplo = 'l';
  double *A;
  MKL_INT *ia, *ja;
  double *x,
    *b,
    *x_sol,
    *Ax,
    *Ax_0;

  double s_initial, s_elapsed, rel_res_nrm2;
  int iter;

  // Process CLI arguments
  struct arguments args;
  cli_opts(argc, argv, &args); f_A = fopen(args.A, "r");
  // END Process CLI arguments

  // Process MatrixMarket file (.mtx)
  mtx_proc(f_A, n, nz);
  // END Process MatrixMarket file (.mtx)

  A = (double *)mkl_malloc(nz * sizeof(double), 64);       // values
  check_allocation(A, "A");
  ia = (MKL_INT *)mkl_malloc(nz * sizeof(MKL_INT), 64);    // colums 
  check_allocation(ia, "ia");
  ja = (MKL_INT *)mkl_malloc((n+1) * sizeof(MKL_INT), 64); // rowIndex
  check_allocation(ja, "ja");
  fill_A(A, ia, ja, n, nz, f_A);
  fclose(f_A);

  x = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(x, "x");
  for (size_t i = 0; i < n; ++i) { *(x + i) = double(rand())/double(RAND_MAX/2) - 1.0; }

  x_sol = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(x_sol, "x_sol");
  for (size_t i = 0; i < n; ++i) { *(x_sol + i) = 1 / sqrt(n); }

  b = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(b, "b");
  for (size_t i = 0; i < n; ++i) { *(b + i) = 1.0; }
  mkl_cspblas_dcsrsymv(&uplo, &n, A, ja, ia, x_sol, b); // b = A * x_sol

  Ax_0 = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(Ax_0, "Ax_0");
  for (size_t i = 0; i < n; ++i) { *(Ax_0 + i) = 1.0; }
  mkl_cspblas_dcsrsymv(&uplo, &n, A, ja, ia, x, Ax_0); // Ax_0 = A * x_0

  cblas_daxpby(n, 1.0, b, 1, -1.0, Ax_0, 1); // Ax_0 = r_0 = b - Ax_0

  iter = (args.imax == 0 ? n : 100);

  s_initial = dsecnd();
  cg(A, ia, ja, &n, x, b, args.tol, iter);
  s_elapsed = dsecnd() - s_initial;

  Ax = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(Ax, "Ax");
  mkl_cspblas_dcsrsymv(&uplo, &n, A, ja, ia, x, Ax); // Ax = A * x
  // y = a*x + b*y | n, a, *x, incx, b, *y, incy
  cblas_daxpby(n, 1.0, b, 1, -1.0, Ax, 1); // Ax = r = b - Ax

  // rel_res_nrm2 = cblas_dnrm2(n, Ax, 1) / cblas_dnrm2(n, Ax_0, 1);
  // print ||r|| and ||r_0||
  printf("||r|| = %12.10lf\n", cblas_dnrm2(n, Ax, 1));
  printf("||r_0|| = %12.10lf\n", cblas_dnrm2(n, Ax_0, 1));

  stats(rel_res_nrm2, iter, s_elapsed);

  printf("\nNo errors\n\n");
  exit(0);
}
