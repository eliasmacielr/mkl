#include "utils.h"

void fill_A(double *A, MKL_INT *ia, MKL_INT *ja, int n, int nz, FILE *f_A)
{
  // A  values.   Contains the nonzero entries of the matrix A.
  // ia columns.  Contains the column indices for each nonzero element of the matrix A.
  // ja rowIndex. Contains the indices of elements in the array A.

  // FILL coordinate matrix
  MKL_INT job[] = {1,  // the matrix in the coordinate format is converted to the CSR format.
                   0,  // zero-based indexing for the matrix in CSR format is used.
                   0,  // zero-based indexing for the matrix in coordinate format is used.
                   0,  // 
                   nz, // maximum number of the nonzero elements allowed if job[0] = 0.
                   0}; // all arrays acsr, ja, ia are filled in for the output storage. 
  double *acoo = (double *)mkl_malloc(nz * sizeof(double), 64);
  check_allocation(acoo, "acoo");
  MKL_INT *rowind = (MKL_INT *)mkl_malloc(nz * sizeof(MKL_INT), 64);
  check_allocation(rowind, "rowind");
  MKL_INT *colind = (MKL_INT *)mkl_malloc(nz * sizeof(MKL_INT), 64);
  check_allocation(colind, "colind");
  MKL_INT info;

  for (size_t i = 0; i < nz; ++i)
    {
      fscanf(f_A, "%d %d %lg\n", &rowind[i], &colind[i], &acoo[i]);
      rowind[i]--;
      colind[i]--;
    }
  // END FILL coordinate matrix

  // PRINT coordinate matrix
  // for (size_t i = 0; i < nz; ++i)
  //   {
  //     printf("%d %d %lg\n", acoo[i], rowind[i], colind[i]);
  //   }
  // printf("\n");
  // END PRINT coordinate matrix

  // CONVERT coordinate acoo, rowind, colind to CSR A, ia, ja matrix
  // void mkl_dcsrcoo(job, n, acsr, ja, ia, nnz, acoo, rowind, colind, info);
  mkl_dcsrcoo(job, &n, A, ia, ja, &nz, acoo, rowind, colind, &info);
  if (info != 0)
    {
      printf("Could not convert coordinate matrix to CSR format.\n");
      printf("info = %d\n", info);
      printf("Terminating program.\n");
      exit(1);
    }
  else
    {
      printf("\n Coordinate matrix converted to CSR format.\n\n");
    }
  // END CONVERT coordinate acoo, rowind, colind to CSR A, ia, ja matrix

  // PRINT CSR matrix
  // for (size_t i = 0; i < nz; ++i)
  //   {
  //     printf("%d %d %lg\n", acoo[i], rowind[i], colind[i]);
  //   }
  // printf("\n");
  // PRINT CSR matrix
}
