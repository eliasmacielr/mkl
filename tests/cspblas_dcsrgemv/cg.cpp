#include "cg.h"

int cg(const double *A, const MKL_INT *ia, const MKL_INT *ja, const MKL_INT *n,
       double *x, const double *b,
       const double tol, int &iter)
{
  char transa = 'n';

  // Allocate memory aligned on a 64-byte boundary for better performance
  double *r, *p, *w;
  r = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(r, "r");
  p = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(p, "p");
  w = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(w, "w");

  double
    alpha, beta,
    rho, rho_old,
    threshold;

  int imax = iter;

  // r = b - A*x
  mkl_cspblas_dcsrgemv(&transa, n, A, ja, ia, x, r); /* r = A*x */
  cblas_daxpby(*n, 1.0, b, 1, -1.0, r, 1); /* r = b - r */
  rho = cblas_ddot(*n, r, 1, r, 1); // rho = (r,r)
  iter = 1;
  
  while (sqrt(rho) > threshold && iter < imax)
    {
      // printf("sqrt(rho) = %10.2f\n", sqrt(rho)); 
      if (iter == 1)
        {
          cblas_dcopy(*n, r, 1, p, 1); // p = r
        }
      else
        {
          beta = rho / rho_old;
          cblas_daxpby(*n, 1.0, r, 1, beta, p, 1); // p = r + beta*p
        }

      mkl_cspblas_dcsrgemv(&transa, n, A, ja, ia, p, w);

      alpha = rho / cblas_ddot(*n, p, 1, w, 1);
      cblas_daxpy(*n, alpha, p, 1, x, 1); // x = x + alpha*p
      cblas_daxpy(*n, -alpha, w, 1, r, 1); // r = r - alpha*w

      rho_old = rho;
      rho = cblas_ddot(*n, r, 1, r, 1); // rho = (r,r)

      ++iter;
    }
}
