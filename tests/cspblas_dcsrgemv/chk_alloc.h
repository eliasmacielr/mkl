#ifndef CHK_ALLOCATION_H
#define CHK_ALLOCATION_H

#include <stdlib.h>
#include <stdio.h>

#include "mkl.h"

void check_allocation(void *, char *);

#endif
