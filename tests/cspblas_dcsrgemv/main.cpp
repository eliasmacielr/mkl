#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "mkl.h"

#include "chk_alloc.h"

#include "cg.h"
#include "opt.h"
#include "mtx_proc.h"
#include "stats.h"


int main(int argc, char **argv)
{
  srand(time(NULL));
  int n, nz;

  FILE *f_A;

  /* Process cli arguments */
  struct arguments args;
  cli_opts(argc, argv, &args); f_A = fopen(args.A, "r");
  /*-------------------*/

  /* Process mtx file */
  mtx_proc(f_A, n, nz);
  /*-------------------*/

  char transa = 'n';
  double *A;
  MKL_INT *ia, *ja;
  double *x,
    *b,
    *x_sol,
    *Ax;

  double s_initial, s_elapsed, rel_res_nrm2;
  int iter;

  // read matrix from file
  int r, c;
  double val;

  MKL_INT m = n;
  MKL_INT info;
  MKL_INT job[] = {0,0,0,2,nz*10,1};

  double *adns = (double *)mkl_malloc(n * n * sizeof(double), 64);
  check_allocation(adns, "adns");
  // fill dense matrix
  for (size_t i = 0; i < nz; ++i)
    {
      fscanf(f_A, "%d %d %lg\n", &r, &c, &val);
      adns[c - 1 + (r - 1)*n] = adns[r - 1 + (c - 1)*n] = val;
    }
  fclose(f_A);
  // end fill dense matrix

  // convert dense adns to sparse A, ia, ja matrix.
  A = (double *)mkl_malloc(nz * sizeof(double), 64);
  check_allocation(A, "A");
  ia = (MKL_INT *)mkl_malloc(nz * sizeof(MKL_INT), 64);
  check_allocation(ia, "ia");
  ja = (MKL_INT *)mkl_malloc((n+1) * sizeof(MKL_INT), 64);
  check_allocation(ja, "ja");
  
  mkl_ddnscsr(job, &m, &n, adns, &n, A, ia, ja, &info);
  if (info != 0)
    {
      printf("Could not convert dense matrix to CSR format.\n");
      printf("info = %d\n", info);
      printf("Terminating program.\n");
      exit(1);
    }
  else
    {
      printf("Dense matrix converted to CSR format.\n");
    }
  // mkl_free(adns);
  // end convert dense adns to sparse A, ia, ja matrix.
  // end read matrix from file
  
  x = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(x, "x");
  for (size_t i = 0; i < n; ++i) { *(x + i) = rand()/10; };

  x_sol = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(x_sol, "x_sol");
  for (size_t i = 0; i < n; ++i) { *(x_sol + i) = 1 / sqrt(n); };

  b = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(b, "b");
  for (size_t i = 0; i < n; ++i) { *(b + i) = 1.0; };
  mkl_cspblas_dcsrgemv(&transa, &n, A, ja, ia, x_sol, b); // b = A * x_sol
  // mkl_free(x_sol); // x_sol is not needed anymore.

  iter = (args.imax == 0 ? n : 100);

  s_initial = dsecnd();
  cg(A, ia, ja, &n, x, b, args.tol, iter);
  s_elapsed = dsecnd() - s_initial;

  Ax = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(Ax, "Ax");
  mkl_cspblas_dcsrgemv(&transa, &n, A, ja, ia, x, Ax);
  cblas_daxpby(n, -1.0, b, 1, 1.0, Ax, 1); // Ax = r
  
  stats(rel_res_nrm2, iter, s_elapsed);

  // free all pointers

  printf("No errors\n");
  exit(0);
}
