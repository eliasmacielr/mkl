#include "stats.h"

void stats(double rel_res_nrm2, int iter, double elapsed_time)
{
  printf("Relative residual [norm(Ax-b)/norm(b)]: %12.2f\n", rel_res_nrm2);
  printf("Iterations: %d\n", iter);
  printf("Elapsed time: %12.2f\n", elapsed_time);
}
