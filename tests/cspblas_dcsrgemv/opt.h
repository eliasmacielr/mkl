#ifndef OPT_H
#define OPT_H

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

/* getopt : Parsing program options 
 * http://www.gnu.org/software/libc/manual/html_node/Getopt.html
 */
#include <getopt.h>

/* Structure to store the values of the arguments accepted by the program.
 * All of them can take default values, so it's not mandatory to pass values for
 * any of them.
 */
struct arguments {
  char *A;
  double tol;
  int imax;
  char *output_file;
};

/* Procedure to process the options and arguments passed to the program 
 * invocation.
 * The first two parameters are the same passed to `main' function, the third 
 * is a pointer to store the result of the options and arguments processing.
 */
void cli_opts(int, char **, struct arguments *);

#endif
