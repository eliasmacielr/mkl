#include "chk_alloc.h"

void check_allocation(void *ptr, char *ptr_name)
{
  if (ptr == NULL) {
    printf("\n ERROR: Can't allocate memory for %s. Aborting... \n\n",
           ptr_name);
    mkl_free(ptr);
    exit(1);
  } else {
    printf("\n %s allocated. \n\n", ptr_name);
  }
}
