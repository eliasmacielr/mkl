#ifndef BCG_H
#define BCG_H

#include <stdio.h>
#include <math.h>
#include <float.h>

#include "mkl.h"

#include "chk_alloc.h"

int bcg(const double *A, const MKL_INT *ia, const MKL_INT *ja, const MKL_INT *n,
        double *X, const MKL_INT *m, const double *b,
       const double tol, int &iter, const int step);

#endif
