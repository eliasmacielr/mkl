#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

#include "mkl.h"

#include "chk_alloc.h"

void fill_A(double *A, MKL_INT *ia, MKL_INT *ja, int n, int nz, FILE *f_A);

#endif
