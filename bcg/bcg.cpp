#include "bcg.h"

int bcg(const double *A, const MKL_INT *ia, const MKL_INT *ja, const MKL_INT *n,
        double *X, const MKL_INT *m, const double *b,
        const double tol, int &iter, const int step)
{
  // A.  values:   Contains the nonzero entries of the matrix A.
  // ia. columns:  Contains the column indices for each nonzero element of the
  //               matrix A. nnz
  // ja. rowIndex: Contains the indices of elements in the array A. (n+1).

  double *R, *P, *W, *Q,
    *e, *rho, *rho_old, *Lambda, *Psi,
    *xi, *eta,
    *r, *x,
    *PTQ, *PPsi,
    *tau;
  double xi_f, threshold;

  char transa;
  double alpha, beta;
  lapack_int *ipiv, *jpvt;
  lapack_int info;

  // Allocate memory aligned on a 64-byte boundary for better performance
  R = (double *)mkl_malloc(*n * *m * sizeof(double), 64);
  check_allocation(R, "R");
  P = (double *)mkl_malloc(*n * *m * sizeof(double), 64);
  check_allocation(P, "P");
  W = (double *)mkl_malloc(*n * *m * sizeof(double), 64);
  check_allocation(W, "W");
  Q = (double *)mkl_malloc(*n * *m * sizeof(double), 64);
  check_allocation(Q, "Q");

  e = (double *)mkl_malloc(*m * sizeof(double), 64);
  check_allocation(e, "e"); for (size_t i = 0; i < *m; ++i) { *(e + i) = 1.0; }
  rho = (double *)mkl_malloc(*m * *m * sizeof(double), 64);
  check_allocation(rho, "rho");
  rho_old = (double *)mkl_malloc(*m * *m * sizeof(double), 64);
  check_allocation(rho_old, "rho_old");
  Lambda = (double *)mkl_malloc(*m * *m * sizeof(double), 64);
  check_allocation(Lambda, "Lambda");
  Psi = (double *)mkl_malloc(*m * *m * sizeof(double), 64);
  check_allocation(Psi, "Psi");

  xi = (double *)mkl_malloc(*m * sizeof(double), 64);
  check_allocation(xi, "xi");
  eta = (double *)mkl_malloc(*m * sizeof(double), 64);
  check_allocation(eta, "eta");

  r = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(r, "r");
  x = (double *)mkl_malloc(*n * sizeof(double), 64);
  check_allocation(x, "x");

  PTQ = (double *)mkl_malloc(*m * *m * sizeof(double), 64);
  check_allocation(PTQ, "PTQ");
  PPsi = (double *)mkl_malloc(*n * *m * sizeof(double), 64);
  check_allocation(PPsi, "PPsi");

  tau = (double *)mkl_malloc(*m * sizeof(double), 64);
  check_allocation(tau, "tau");

  ipiv = (lapack_int *)mkl_malloc(*m * sizeof(lapack_int), 64);
  check_allocation(ipiv, "ipiv");
  jpvt = (lapack_int *)mkl_malloc(*m * sizeof(lapack_int), 64);
  check_allocation(jpvt, "jpvt"); for (size_t i = 0; i < *m; ++i) { *(jpvt + i) = 0; }

  int imax = iter;

  // R = b*e' - A*X
  for (size_t i = 0; i < *n**m; ++i) { *(R + i) = 0.0; }
  cblas_dger(CblasRowMajor, *n, *m, 1.0, b, 1, e, 1, R, *m); // R := 1.0*b*e' + R
  char matdescra[6] = {'S', 'L', 'N', 'C'}; transa = 'n'; alpha = -1.0; beta = 1.0;
  mkl_dcsrmm(&transa, n, m, n, &alpha, matdescra, A, ia, ja, ja+1, X, m, &beta, R, m); // R = R - A*X || R := -1.0*A*X + 1.0R
  // rho = R' * R || rho := 1.0*R'*R + 0.0*rho
  cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, *m, *m, *n, 1.0, R, *m, R, *m, 0.0, rho, *m);

  iter = 1;

  threshold = tol * cblas_dnrm2(*n, b, 1);

  while (iter < imax)
    {
      if (iter % step == 0)
        {
          //
          printf("iter = %d\n", iter);
          // 
          // [~,W] = qr(R,0)
          mkl_domatcopy('r', 'n', *n, *m, 1.0, R, *m, W, *m); // W = R
          info = LAPACKE_dgeqp3(LAPACK_ROW_MAJOR, *n, *m, W, *m, jpvt, tau); // W: upper triangular matrix
          if (info != 0) { return info; }
          // xi = W' \ (W \ e)
          cblas_dcopy(*m, e, 1, eta, 1); // eta = e
          cblas_dtrsv(CblasRowMajor, CblasUpper, CblasNoTrans, CblasNonUnit, *m, W, *m, eta, 1); // W*eta = e
          cblas_dcopy(*m, eta, 1, xi, 1); // xi = eta
          cblas_dtrsv(CblasRowMajor, CblasUpper, CblasTrans, CblasNonUnit, *m, W, *m, xi, 1); // W'*xi = eta
          // xi_f = 1.0 / (e' * xi)
          xi_f = 1.0 / cblas_ddot(*m, e, 1, xi, 1);
          // r = xi_f * R * xi
          cblas_dgemv(CblasRowMajor, CblasNoTrans, *n, *m, xi_f, R, *m, xi, 1, 0.0, r, 1);
          printf("cblas_dnrm2(*n, r, 1) = %.*e\n", DECIMAL_DIG, cblas_dnrm2(*n, r, 1));
          if (cblas_dnrm2(*n, r, 1) <= threshold)
            {
              // x = xi_f * X * xi
              cblas_dgemv(CblasRowMajor, CblasNoTrans, *n, *m, xi_f, X, *m, xi, 1, 0.0, x, 1);
              return 0;
            }
        }

      if (iter == 1)
        {
          mkl_domatcopy('r', 'n', *n, *m, 1.0, R, *m, P, *m); // P = R || P := 1.0*R
        }
      else
        {
          // Psi = rho_old \ rho
          mkl_domatcopy('r', 'n', *m, *m, 1.0, rho, *m, Psi, *m); // Psi = rho || Psi := 1.0*rho
          info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, *m, *m, rho_old, *m, ipiv, Psi, *m);
          if (info != 0) { return info; }

          // P = R + P * Psi
          // PPsi = P * Psi || PPsi := 1.0*P*Psi + 0.0*PPsi
          cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, *n, *m, *m, 1.0, P, *m, Psi, *m, 0.0, PPsi, *m);
          // P := 1.0*R + 1.0*PPsi
          mkl_domatadd('r', 'n', 'n', *n, *m, 1.0, R, *m, 1.0, PPsi, *m, P, *m);
        }

      // Q = A * P || Q := 1.0*A*P + 0.0*Q
      transa = 'n'; alpha = 1.0; beta = 0.0;
      mkl_dcsrmm(&transa, n, m, n, &alpha, matdescra, A, ia, ja, ja+1, X, m, &beta, Q, m);

      // Lambda = (P' * Q) \ rho
      // PTQ = P' * Q || PTQ := 1.0*P'*Q + 0.0*PTQ
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, *m, *m, *n, 1.0, P, *m, Q, *m, 0.0, PTQ, *m);
      // Lambda = PTQ \ rho
      mkl_domatcopy('r', 'n', *m, *m, 1.0, rho, *m, Lambda, *m); // Lambda = rho
      info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, *m, *m, PTQ, *m, ipiv, Lambda, *m);
      if (info != 0) { return info; }

      // X = X + P * Lambda || X := 1.0*P*Lambda + 1.0*X
      cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, *n, *m, *m, 1.0, P, *m, Lambda, *m, 1.0, X, *m);

      // R = R - Q * Lambda || R := (-1.0)*Q*Lambda + 1.0*R
      cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, *n, *m, *m, -1.0, Q, *m, Lambda, *m, 1.0, R, *m);

      mkl_domatcopy('r', 'n', *m, *m, 1.0, rho, *m, rho_old, *m); // rho_old = rho

      // rho = R' * R || rho := 1.0*R'*R + 0.0*rho
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, *m, *m, *n, 1.0, R, *m, R, *m, 0.0, rho, *m);

      ++iter;
    }

  // [~,W] = qr(R,0)
  mkl_domatcopy('r', 'n', *n, *m, 1.0, R, *m, W, *m); // W = R
  info = LAPACKE_dgeqp3(LAPACK_ROW_MAJOR, *n, *m, W, *m, jpvt, tau); // W: upper triangular matrix
  if (info != 0) { return info; }
  // xi = W' \ (W \ e)
  cblas_dcopy(*m, e, 1, eta, 1); // eta = e
  cblas_dtrsv(CblasRowMajor, CblasUpper, CblasNoTrans, CblasNonUnit, *m, W, *m, eta, 1); // W*eta = e
  cblas_dcopy(*m, eta, 1, xi, 1); // xi = eta
  cblas_dtrsv(CblasRowMajor, CblasUpper, CblasTrans, CblasNonUnit, *m, W, *m, xi, 1); // W'*xi = eta
  // xi_f = 1.0 / (e' * xi)
  xi_f = 1.0 / cblas_ddot(*m, e, 1, xi, 1);
  // r = xi_f * R * xi
  cblas_dgemv(CblasRowMajor, CblasNoTrans, *n, *m, xi_f, R, *m, xi, 1, 0.0, r, 1);
  // x = xi_f * X * xi
  cblas_dgemv(CblasRowMajor, CblasNoTrans, *n, *m, xi_f, X, *m, xi, 1, 0.0, x, 1);

  return 0;
}
