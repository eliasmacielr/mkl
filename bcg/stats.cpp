#include "stats.h"

void stats(double rel_res_nrm2, int iter, double elapsed_time)
{
  printf("Relative residual [norm(Ax-b)/norm(b)]:\n %lf\n", rel_res_nrm2);
  printf("Iterations:\n %12d\n", iter);
  printf("Elapsed time:\n %12.2lfs\n", elapsed_time);
}
