#ifndef CHK_ALLOC_H
#define CHK_ALLOC_H

#include <stdlib.h>
#include <stdio.h>

#include "mkl.h"

void check_allocation(void *, char *);

#endif
