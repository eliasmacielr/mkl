#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "mkl.h"

#include "bcg.h"
#include "chk_alloc.h"
#include "mtx_proc.h"
#include "opt.h"
#include "stats.h"
#include "utils.h"


int main(int argc, char **argv)
{
  srand(time(NULL));
  int n, m, nz;

  FILE *f_A;

  char uplo;
  double *A;
  MKL_INT *ia, *ja;
  double *X,
    *b,
    *x_sol,
    *Ax,
    *Ax_0;

  double s_initial, s_elapsed, rel_res_nrm2;
  int iter;

  // Process CLI arguments
  struct arguments args;
  cli_opts(argc, argv, &args); f_A = fopen(args.A, "r");
  // END Process CLI arguments

  // Process MatrixMarket file (.mtx)
  mtx_proc(f_A, n, nz);
  // END Process MatrixMarket file (.mtx)
  m = args.m;

  A = (double *)mkl_malloc(nz * sizeof(double), 64);       // values
  check_allocation(A, "A");
  ia = (MKL_INT *)mkl_malloc(nz * sizeof(MKL_INT), 64);    // colums 
  check_allocation(ia, "ia");
  ja = (MKL_INT *)mkl_malloc((n+1) * sizeof(MKL_INT), 64); // rowIndex
  check_allocation(ja, "ja");
  fill_A(A, ia, ja, n, nz, f_A);
  fclose(f_A);

  X = (double *)mkl_malloc(n * m * sizeof(double), 64);
  check_allocation(X, "X");
  for (size_t i = 0; i < n*m; ++i) { *(X + i) = double(rand())/double(RAND_MAX/2) - 1.0; }

  x_sol = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(x_sol, "x_sol");
  for (size_t i = 0; i < n; ++i) { *(x_sol + i) = 1 / sqrt(n); }

  b = (double *)mkl_malloc(n * sizeof(double), 64);
  check_allocation(b, "b");
  uplo = 'l';
  mkl_cspblas_dcsrsymv(&uplo, &n, A, ja, ia, x_sol, b); // b = A * x_sol

  iter = (args.imax == 0 ? n : 100);

  s_initial = dsecnd();
  lapack_int info = bcg(A, ia, ja, &n, X, &m, b, args.tol, iter, args.step);
  s_elapsed = dsecnd() - s_initial;
  if (info != 0)
    {
      printf("Error: %d\n", info);
      exit(1);
    }

  rel_res_nrm2 = 0; // dummy value
  stats(rel_res_nrm2, iter, s_elapsed);

  printf("\nNo errors\n\n");
  exit(0);
}
